import './App.css';
import Main from './components/Main';
import Navbar from './components/navbar';

function App() {
  return (
    <div>
      <Navbar />
      <Main />

    </div>
  );
}

export default App;
