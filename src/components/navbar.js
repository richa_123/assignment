import React from 'react';
import AddCircleIcon from '@mui/icons-material/AddCircle';
import ArrowCircleLeftIcon from '@mui/icons-material/ArrowCircleLeft';
function navbar() {
  return (
    <div>
      <nav className="navbar card">
        <div className="container-fluid">
          <span className="p-1"><ArrowCircleLeftIcon /></span>
          <div className="col">
            <a className="navbar-brand" href="#">Back</a>
            <a className="navbar-brand btn btn-primary button" href="#">B2C</a>
            <a className="navbar-brand" href="#">B2B Tax</a>
            <a className="navbar-brand" href="#">B2B Supply</a>
            <a className="navbar-brand" href="#">Manual</a>
            <a className="navbar-brand" href="#">
              <span><AddCircleIcon /></span>
            </a>
          </div>
          <div>
            <a className="navbar-brand" href="#">Bill Documentation</a>
          </div>

        </div>
      </nav>
    </div>
  )
}

export default navbar
