import React from 'react';
import '../App.css';
import AddCircleIcon from '@mui/icons-material/AddCircle';
import HelpOutlineIcon from '@mui/icons-material/HelpOutline';
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye';
import PrintIcon from '@mui/icons-material/Print';
import NoteAddIcon from '@mui/icons-material/NoteAdd';
import EmailIcon from '@mui/icons-material/Email';
import PhoneIphoneIcon from '@mui/icons-material/PhoneIphone';
import CheckIcon from '@mui/icons-material/Check';
import ClearIcon from '@mui/icons-material/Clear';
import MemoryIcon from '@mui/icons-material/Memory';
function Main() {
    return (
        <div className="container-fluid pt-4">
            <div className='row'>
                <div className="col-2">
                    <div className="card m-4">
                        <div className="card-body text-center">
                            <a href="#">Unit Mapped 2/725</a>
                        </div>
                    </div>
                </div>
                <div className="col-2">
                    <div className="card m-4 text-center">
                        <div className="card-body">
                            <a href="#">Active charges 7/7</a>
                        </div>
                    </div>
                </div>
                <div className="col-2">
                    <div className="card m-4 text-center ">
                        <div className="card-body">
                            <a href="#">Reading  -/2</a>
                        </div>
                    </div>
                </div>
                {/* this code for table */}
                <div className='col-5'>
                    <table className="table table-bordered">
                        <tr>
                            <th className='heading p-2 text-center'>Billing Cycle</th>
                        </tr>
                        <tr>
                            <th className='p-2'>Billing Period FROM|TO</th>
                            <td className='p-2'>01 Nov 2021 |30 Nov 2021</td>
                        </tr>
                        <tr>
                            <th className='p-2'>Bill due date</th>
                            <td className='p-2'>1 Jan 2022 | 01 Jan 2022</td>
                        </tr>
                        <tr>
                            <th className='p-2'>Consider Payment To</th>
                            <td className='p-2'>30 Nov 2021</td>
                        </tr>
                        <tr>
                            <th className='p-2'>Consider meter reading FROM|TO</th>
                            <td className='p-2'>01 Nov 2021 |30 Nov 2021</td>
                        </tr>
                    </table>
                </div>
            </div>
            {/* this code for form */}
            <div className='form-row'>
                <div className='row'>
                    <div className='form-group col-sm-3 pt-3'>
                        <h6>Billing cycle *<span><AddCircleIcon /><i class="fa-regular fa-pen-to-square"></i></span></h6>
                        <select className="form-select form-select-sm" aria-label=".form-select-sm example">
                            <option selected>Nov-21-B2C | B2C</option>
                            <option value="1">Nov-21-B2C | B2C</option>
                            <option value="2">Nov-21-B2C | B2C</option>
                            <option value="3">Nov-21-B2C | B2C</option>
                        </select>
                        <h6 className="head">Billing cycle is active.<button className='check mt-1'><CheckIcon /></button></h6>
                    </div>
                    <div className='form-group col-sm-3 pt-3'>
                        <h6>Cluster</h6>
                        <select className="form-select form-select-sm" aria-label=".form-select-sm example"><button><ClearIcon /></button>
                            <option selected>AMN</option>
                            <option value="1">NNO</option>
                            <option value="2">EEO</option>
                            <option value="3">RRO</option>

                        </select>

                    </div>

                </div>
                <div className='row'>
                    <div className='form-group col-sm-3 pt-3'>
                        <h6>Tower</h6>
                        <select className="form-select form-select-sm" aria-label=".form-select-sm example"><span><ClearIcon /></span>
                            <option selected>Select Tower</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                        </select>
                    </div>
                    <div className='form-group col-sm-3 pt-3'>
                        <h6>Unit</h6>
                        <select className="form-select form-select-sm" aria-label=".form-select-sm example">
                            <option selected>AMN0020905-Lokendra Kumar Sharma</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                        </select>
                    </div>

                </div>
            </div>
            <div className='form-group col-sm-3 pt-3'>
                <input placeholder='Remarks' />

            </div>
            <div class="form-check pt-4">
                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                <h6>
                    Provisional Bill<HelpOutlineIcon />
                </h6>
            </div>
            <div className='form-row'>
                <div className='row'>
                    <div class="btn-group btn-group-sm col-sm-6">
                        <button type="button" class="btn btn-danger"><MemoryIcon />Generate</button>
                        <button type="button" class="btn btn-primary"><RemoveRedEyeIcon />Preview</button>
                        <button type="button" class="btn btn-light icon" ><NoteAddIcon /></button>
                        <button type="button" class="btn btn-primary"><PrintIcon />Print</button>
                        <button type="button" class="btn btn-light icon"><NoteAddIcon /></button>
                    </div>
                    <div class="btn-group btn-group-sm col-sm-3">
                        <button type="button" class="btn btn-primary"><EmailIcon /></button>
                        <button type="button" class="btn btn-light icon"><RemoveRedEyeIcon /></button>
                        <button type="button" class="btn btn-light icon"><PrintIcon /></button>
                        <button type="button" class="btn btn-primary"><PhoneIphoneIcon />SMS</button>
                        <button type="button" class="btn btn-light icon"><RemoveRedEyeIcon /></button>
                    </div>
                </div>
            </div>
        </div>




    )
}

export default Main
